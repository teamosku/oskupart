/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioht;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Osku
 */
public class Cities {
    private ArrayList<City> cityList = new ArrayList<>();       //list of all cities
    private ArrayList<City> citiesOnMap = new ArrayList<>();    //list of cities user has added to map
    private static Cities cities = null;
    private Document doc;
    private Cities(){
        addData();   
    }
    public static Cities getInstance(){
        if (cities == null){
            cities = new Cities();
        }
        return cities;
    }
    private void addData(){
        try {
            System.out.println("Loading data...");
            String line, content = "";
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            while ((line = br.readLine()) != null){                 //getting the data
                content += line + "\n";
            }
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();       
            doc = dBuilder.parse(new InputSource(new StringReader(content)));   //adding the data to document
            parseDocument();
        } catch (MalformedURLException ex) {
            Logger.getLogger(Cities.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            Logger.getLogger(Cities.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void parseDocument() {
        System.out.println("Starting to parse...");
        NodeList nodes = doc.getElementsByTagName("place");     //getting all cities
        Element e = doc.getDocumentElement();
        String cityName;
	if(nodes != null && nodes.getLength() > 0) {
	    for(int i = 0 ; i < nodes.getLength();i++) {
                Element el = (Element)nodes.item(i);
                cityName = getTextValue(el, "city");
                SmartPost sp = new SmartPost(getTextValue(el, "postoffice"), getTextValue(el, "availability"), cityName     //creating smartpostmachine
                        , getTextValue(el, "address"), getTextValue(el, "lat"), getTextValue(el, "lng")
                        , getTextValue(el, "code"));
                if (hasCity(cityName)){     //if city already exists in list
                    getCity(cityName).addSmartPost(sp);
                }else{                      //if first machine in city
                    City c = new City(cityName);
                    cityList.add(c);
                    c.addSmartPost(sp);
                }
            }
        }
        System.out.println("...Parsing finished");
    }
    private String getTextValue(Element ele, String tagName) {      //returns elements string value
	String textVal = null;
	NodeList nl = ele.getElementsByTagName(tagName);
	if(nl != null && nl.getLength() > 0) {
            Element el = (Element)nl.item(0);
            textVal = el.getFirstChild().getNodeValue();
	}
	return textVal;
    }
    private boolean hasCity(String s){      //checks if cityList contains a certain city, smart netbeans reduced this method to one line >_>
        return cityList.stream().anyMatch((c) -> (c.getName().equals(s)));
    }
    private City getCity(String city){
        for (City c : cityList){
            if (c.getName().equals(city))
                return c;
        }
        return null;
    }
    public ArrayList<City> getCityList(){
        return cityList;
    }
    public ArrayList<City> getCitiesOnMap(){
        return citiesOnMap;
    }
    public void addCitiesOnMap(City c){
        if (!hasCityOnMap(c.getName())){
            citiesOnMap.add(c);
        }
    }
    private boolean hasCityOnMap(String s){     //checks if citiesOnMap contains a certain city
        return citiesOnMap.stream().anyMatch((c) -> (c.getName().equals(s)));
    }
}
