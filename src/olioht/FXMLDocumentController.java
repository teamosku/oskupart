/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioht;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author Osku
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private WebView googleMaps;
    @FXML
    private ComboBox<City> cityCombo;
    @FXML
    private Button makePackageButton;
    @FXML
    private Button editPackageButton;
    @FXML
    private ComboBox<Package> packageCombo;
    @FXML
    private Button sendPackageButton;
    @FXML
    private Button addCityButton;
    @FXML
    private Button removePathsAction;
    Storage storage;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {  
        googleMaps.getEngine().load(getClass().getResource("index.html").toExternalForm()); //loads recources
        Cities cities = Cities.getInstance();
        for (City c: cities.getCityList()){ //updates combo for all cities
            cityCombo.getItems().add(c);
        }
        storage = Storage.getInstance();
        updateCombo();
    }    

    @FXML
    private void makePackageAction(ActionEvent event) { //make new package
        loadPackage();
    }

    @FXML
    private void editPackageAction(ActionEvent event) { //edit existing package
        if(packageCombo.getSelectionModel().getSelectedIndex()==-1) popup("Valitse paketti");
        else{
            storage.setMustEdit(packageCombo.getSelectionModel().getSelectedIndex());
            loadPackage();
        }
    }

    @FXML
    private void sendPackageAction(ActionEvent event) { //TIMO embarks on his noble journey
        if(packageCombo.getSelectionModel().getSelectedIndex()==-1) popup("Valitse paketti");
        else{
            Package p = storage.getPackage(packageCombo.getSelectionModel().getSelectedIndex());
            String s1 = "["+p.getStartLat()+", "+p.getStartLon()+", "+p.getEndLat()+", "+p.getEndLon()+"]"; //getting the arguments
            String s2 = "'blue'";
            String s3 = p.getSpeed().toString();
            double i = (double) googleMaps.getEngine().executeScript("document.createPath("+s1+", "+s2+", "+s3+")");    //running javascript to creathe path
            if (i > 150.0 && s3.equals("3")){   //class 1 package has speed 3 and can only be sent for 150km max.
                removePathsAction(event);       //yes, I know it removes all paths >_>, no-idea how to just check distance without drawing anything
                popup("TIMO-poika kieltäytyy juoksemasta " + i + "km matkaa.");
            }else{
                if (p.getMailObject().isBroken()){
                    popup("Myöhemmin päivällä saatiin valitus rikkinäisestä toimituksesta.");
                }
                storage.removePackage(packageCombo.getSelectionModel().getSelectedIndex());             //removin sent package from storage
                updateCombo();
            }
        }
    }

    @FXML
    private void removePathsAction(ActionEvent event) {
        googleMaps.getEngine().executeScript("document.deletePaths()"); //removing routs
    }

    @FXML
    private void addCityAction(ActionEvent event) { //adding new smartpost machines to map
        City c = cityCombo.getValue();
        Cities cities = Cities.getInstance();
        cities.addCitiesOnMap(c);
        for (SmartPost sp : c.getSmartPostList()){
            String s1 = "'" + sp.getAddress() + ", " + sp.getCode() + " " + sp.getCity().toLowerCase() + "'";
            String s2 = "'" + sp.getName() + " Avoinna: " +sp.getAvail() + "'";
            String s3 = "'blue'";
            googleMaps.getEngine().executeScript("document.goToLocation("+s1+", "+s2+", "+s3+")");
        }
    }
    
    private void popup(String s){
        try {
            Stage webview = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLpopup.fxml"));
            Scene scene = new Scene(page);
            webview.setScene(scene);
            webview.show();
            webview.setTitle(s);
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void loadPackage(){ //opening a new window for package management
        try {
            Stage webview = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLPackage.fxml"));
            Scene scene = new Scene(page);
            webview.setScene(scene);
            webview.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @FXML
    private void updateCombo(MouseEvent event) {
        updateCombo();
    }
    private void updateCombo(){ //updates items in package combo
        ObservableList s = FXCollections.observableArrayList(storage.getPackages());
        packageCombo.setItems(s);
    }
}
