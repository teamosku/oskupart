/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioht;

import java.util.ArrayList;

/**
 *
 * @author Skiivari
 */
public abstract class Package {
    final private String startLat,startLon,endLat,endLon;
    final private int speed;
    private MailObject object;
    
    Package(String sla, String slo, String ela, String elo, int s){
        startLat=sla;
        startLon=slo;
        endLat=ela;
        endLon=elo;
        speed=s;
    }
    public int getRate(){
        int rate = 1;
        if(speed==1) rate = 3;
        if(speed==2) rate = 2;
        return rate;
    }
    
    public MailObject getMailObject(){
        return object;
    }


    public String getStartLat() {
        return startLat;
    }

    public String getStartLon() {
        return startLon;
    }

    public String getEndLat() {
        return endLat;
    }

    public String getEndLon() {
        return endLon;
    }
    
    public Integer getSpeed() {
        return speed;//Working with an Integer, not int
    }
    
    public void breakContents(){
        object.breakObject();
    }
    
    @Override
    public String toString(){
        try{String n="Paketoitu " + object.toString();
            return n;
        }
        catch (Exception x){
            return "";
        }
    }
    
    public static class FirstClass extends Package {
        public FirstClass(String sla, String slo, String ela, String elo, MailObject mo){
           super(sla,slo,ela,elo,3);
           super.object = mo;
        }
        public FirstClass(){
           super("default","default","default","default",1);
        }
    }
    public static class SecondClass extends Package {
        public SecondClass(String sla, String slo, String ela, String elo, MailObject mo){
           super(sla,slo,ela,elo,2);
           super.object = mo;
        }
    }
    public static class ThirdClass extends Package {
        public ThirdClass(String sla, String slo, String ela, String elo, MailObject mo){
           super(sla,slo,ela,elo,1);
           super.object = mo;
        }
    }
}
