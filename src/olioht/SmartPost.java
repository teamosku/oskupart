/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioht;

/**
 *
 * @author Osku
 */
public class SmartPost {
    private String avail;   //open hours
    private String city;    //city name where smartpost is located
    private String address;
    private String name;    //smartpost machine name
    private String lat, lon;    //latitude and longitude
    private String code;        //smartposts' regions' postalcode
    public SmartPost(String n, String a, String c, String ad, String la, String lo, String cd){
        name = n;
        avail = a;
        city = c;
        address = ad;
        lat = la;
        lon = lo;
        code = cd;
    }
    public SmartPost(){
        name = "default";
        avail = "default";
        city = "default";
        address = "default";
        lat = "default";
        lon = "default";
        code = "default";   
    }
    public String getLat(){
        return lat;
    }
    public String getLon(){
        return lon;
    }
    public String getCity() {
        return city;
    }
    public String getAddress() {
        return address;
    }
    public String getCode() {
        return code;
    }
    public String getAvail() {
        return avail;
    }
    public String getName() {
        return name;
    }
    @Override
    public String toString(){
        return name;
    }
}
