/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioht;

import java.util.ArrayList;



/**
 *
 * @author Skiivari
 */
public class MailObject {
    private double x,y,z,w;
    private String name;
    private boolean fragile;
    private boolean broken=false;
    
    public MailObject(){
    }
    
    public ArrayList<Double> getDimW(){
        ArrayList<Double> al = new ArrayList();
        al.add(x);
        al.add(y);
        al.add(z);
        al.add(w);
        return al;
    }
    
    public boolean isFragile(){
        return fragile;
    }

    public MailObject(String n, double width, double length, double height, double weight, boolean f){
        name = n;
        x = width;
        y = length;
        z = height;
        w = weight;
        fragile = f;
    }
    
    
    public void breakObject(){
        broken = true;
    }
    public boolean isBroken(){
        return broken;
    }
    
    class Hat extends MailObject{
        private Hat(){
            super("Partyhat",20,20,10,0.04,false);
        }
    }
    
    class Turbo extends MailObject{
        private Turbo(){
            super("HX35",20,20,30,5,false);

        }
    }
    
    class Porcelain extends MailObject{
        private Porcelain(){
            super("Posliinit",10,10,16,0.74,true);
        }
    }
    
    class Pistol extends MailObject{
        private Pistol(){
            super("911 Dual Magnum",3,25,15,1.45,false);
        }  
    }
    
    public ArrayList<MailObject> getObjectList(){
        ArrayList<MailObject> ol = new ArrayList<>();
        ol.add(new Turbo());
        ol.add(new Hat());
        ol.add(new Porcelain());
        ol.add(new Pistol());
        return ol;
    }
    @Override
    public String toString(){
        return name;
    }
    
}