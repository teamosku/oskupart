/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioht;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.lang.Integer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * FXML Controller class
 *
 * @author Osku
 */
public class FXMLPackageController implements Initializable {
    @FXML
    private Button rateInfoButton;
    @FXML
    private ComboBox<MailObject> moCombo;
    @FXML
    private ComboBox<SmartPost> destMachine;
    @FXML
    private ComboBox<City> destCityCombo;
    @FXML
    private ComboBox<City> depCityCombo;
    @FXML
    private ComboBox<SmartPost> depMachine;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField massField;
    @FXML
    private Button finishButton;
    @FXML
    private Button cancelButton;
    @FXML

    private CheckBox isFragile;
    @FXML
    private ComboBox<Integer> rateCombo;
    int editable = -1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Cities cities = Cities.getInstance();
        for(City c:cities.getCitiesOnMap()){
            destCityCombo.getItems().add(c);
            depCityCombo.getItems().add(c);
        }
        MailObject moget = new MailObject();
        for(MailObject mo : (moget.getObjectList())){
            moCombo.getItems().add(mo);
        }
        rateCombo.getItems().add(1);
        rateCombo.getItems().add(2);
        rateCombo.getItems().add(3);
        //When Editing:
        Storage storage = Storage.getInstance();
        
        if(storage.mustEdit() != -1){
            Package p = storage.getPackage(storage.mustEdit());
            for(City c:depCityCombo.getItems()){
                for(SmartPost sp:c.getSmartPostList()){
                    if (p.getStartLon()==sp.getLon() && p.getStartLat()==sp.getLat()){
                        depMachine.getSelectionModel().select(sp);
                        depCityCombo.getSelectionModel().select(c);
                    }
                }
            }
            for(City c:destCityCombo.getItems()){
                for(SmartPost sp:c.getSmartPostList()){
                    if (p.getEndLon()==sp.getLon() && p.getEndLat()==sp.getLat()){
                        destMachine.getSelectionModel().select(sp);
                        destCityCombo.getSelectionModel().select(c);
                    }
                }
            }
            MailObject mo = p.getMailObject();
            rateCombo.getSelectionModel().select(p.getRate()-1);
            nameField.setText(mo.toString());
            ArrayList<Double> al = mo.getDimW();
            sizeField.setText(al.get(0).toString()+"x"+al.get(1).toString()+"x"+al.get(2).toString());
            massField.setText(al.get(3).toString());
            isFragile.selectedProperty().set(mo.isFragile());
        editable = storage.mustEdit();
        storage.setMustEdit(-1);
        }
    }

    @FXML
    private void showInfoPopup(ActionEvent event) {
        try {
            Stage webview = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLinfo.fxml"));
            Scene scene = new Scene(page);
            webview.setScene(scene);
            webview.show(); 
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @FXML
    private void loadDepMachinesAction(ActionEvent event) {
        depMachine.getItems().clear();
        for(SmartPost sp:depCityCombo.getSelectionModel().getSelectedItem().getSmartPostList()){
            depMachine.getItems().add(sp);
        }
    }
    
    @FXML
    private void loadDestMachinesAction(ActionEvent event) {
        destMachine.getItems().clear();
        for(SmartPost sp:destCityCombo.getSelectionModel().getSelectedItem().getSmartPostList()){
            destMachine.getItems().add(sp);
        }
    }

    @FXML
    private void createPackageAction(ActionEvent event) {
        SmartPost depSP = new SmartPost();
        SmartPost destSP = new SmartPost();
        MailObject mo = new MailObject();
        Boolean out = false;
        try{
            
            if(moCombo.getValue()!=null){
                mo=moCombo.getSelectionModel().getSelectedItem();
            }
            else{
                String n = nameField.getText();
                String sizeString = sizeField.getText();
                double w = Double.parseDouble(massField.getText());
                String[] parts = sizeString.split("x");
                mo = new MailObject(n, Double.parseDouble(parts[0]), Double.parseDouble(parts[1]), Double.parseDouble(parts[2]), w, isFragile.isSelected());
            }
            depSP = depMachine.getSelectionModel().getSelectedItem();
            destSP = destMachine.getSelectionModel().getSelectedItem();
        }
        
        catch (Exception ex){
            if (ex.getMessage().length()==1) popup("Tarkista mittojen formatointi");
            else popup(ex.getMessage());
            return;
        }
        Storage storage = Storage.getInstance();
        Package p = new Package.FirstClass();
        switch(rateCombo.getSelectionModel().getSelectedIndex()){   
            
            case 0: 
                try{
                    p = new Package.FirstClass(depSP.getLat(), depSP.getLon(), destSP.getLat(), destSP.getLon(), mo);
                }catch(Exception ex){
                    if (ex.getMessage()==null) popup("Tarkista SmartPost-automaatit");
                    else popup(ex.getMessage());
                    return;
                }break;
            case 1:
                try{
                    p = new Package.SecondClass(depSP.getLat(), depSP.getLon(), destSP.getLat(), destSP.getLon(), mo);
                }catch(Exception ex){
                    if (ex.getMessage()==null) popup("Tarkista SmartPost-automaatit");
                    else popup(ex.getMessage());
                    return;
                }break;
            case 2:
                try{
                    p = new Package.ThirdClass(depSP.getLat(), depSP.getLon(), destSP.getLat(), destSP.getLon(), mo);
                }catch(Exception ex){
                    if (ex.getMessage()==null) popup("Tarkista SmartPost-automaatit");
                    else popup(ex.getMessage());
                    return;
                }break;
            default: 
                popup("Valitse lähetysluokka!");
                return;
        }
        if (editable!=-1){
            storage.removePackage(editable);
        }
        storage.addPackage(p);
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void cancelAction(ActionEvent event) {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }
    
    private void popup(String s){
        try {
            Stage webview = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLpopup.fxml"));
            Scene scene = new Scene(page);
            webview.setScene(scene);
            webview.show();
            webview.setTitle("Virhe: " +s);
        } catch (IOException ex) {
            Logger.getLogger(FXMLPackageController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
