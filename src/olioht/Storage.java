package olioht;

import java.util.ArrayList;

/**
 *
 * @author Skiivari
 */
public enum Storage {
    INSTANCE; //makes class a singleton
    private final ArrayList<Package> storage = new ArrayList();
    private int mustEdit = -1;
    
    public int mustEdit(){
        return mustEdit;
    }
    public void setMustEdit(int i){
        mustEdit = i;
    }
    public static Storage getInstance()
        {
            return INSTANCE;
        }
    
    public Package getPackage(int i){
        return storage.get(i);    
    }
    
    public void removePackage(int i){
        storage.remove(i);
    }
    
    public void addPackage(Package p){
        storage.add(p);
    }
    
     public ArrayList<String> getPackages(){
        ArrayList<String> l = new ArrayList();
         for(Package p:storage){
            l.add(p.toString());
        }
        return l;
     }
    
    
    
}
