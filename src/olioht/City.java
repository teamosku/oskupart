/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olioht;

import java.util.ArrayList;

/**
 *
 * @author Osku
 */
public class City {
    private String name;
    private ArrayList<SmartPost> smartPostList = new ArrayList<>();     //list of smartpost machines in this city
    public City(String n){
        name = n;
    }
    public void addSmartPost(SmartPost sp){
        smartPostList.add(sp);
    }
    public ArrayList<SmartPost> getSmartPostList(){
        return smartPostList;
    }
    public String getName(){
        return name;
    }
    @Override
    public String toString(){
        return name;
    }
}
